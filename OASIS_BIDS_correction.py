#!/usr/bin/env python
#-*- coding: utf-8 -*-

###################################################################

__author__ = 'Dhivya Srinivasan'


## Import necessary modules

import numpy as np
import pandas as pd
import os
import sys
import getopt
import json

#####################################################################
EXEC_NAME = "OASIS_BIDS_correction.py"
def help():
    """
    usage information
    """
    print(r"""
  %(EXEC)s--

Usage : %(EXEC)s [OPTIONS]

    A Script to convert downloaded OASIS data to be compliant with BIDS new format.
    Background: OASIS BIDS formatted data were downloaded using
    https://github.com/NrgXnat/oasis-scripts/tree/master/download_scans/download_oasis_scans_bids.sh
    They were using older BIDS format which is not compliant with new version. To make them compatible,
    https://github.com/NrgXnat/oasis-scripts/issues/11#issuecomment-680812556
    https://bids-specification.readthedocs.io/en/stable/04-modality-specific-files/01-magnetic-resonance-imaging-data.html#fieldmap-data

    1. dataset_description.json should be placed in the folder above sub-OAS.
    2. Remove dataset_description.json from each subject's folder
    3. Change naming scheme of field map files with two magnitude and one phase difference file.
    Example:
    sub-OAS30002_ses-d2345_run-02_fieldmap.nii.gz ---> sub-OAS30002_ses-d2345_run-01_phasediff.nii.gz
    sub-OAS30002_ses-d2345_echo-1_run-01_fieldmap.nii.gz ---> sub-OAS30002_ses-d2345_run-01_magnitude1.nii.gz
    sub-OAS30002_ses-d2345_echo-2_run-01_fieldmap.nii.gz ---> sub-OAS30002_ses-d2345_run-01_magnitude2.nii.gz
    4. Add EchoTime1 and EchoTime2 fields to the phase difference JSON sidecar file and remove EchoTime.

    File with longer echo is magnitude2 and Echo2.

  Required Options:
    [-i --echoFile1]   Specify the echo1 file name (.nii.gz & run-01)
    [-j --echoFile2]   Specify the echo2 File name (.nii.gz & run-01)
    [-p --phasediff]   Specify the name with run-02 fieldmap (.nii.gz)
    [-k --inDir]        Specify path to input directory - subject and session level

    Options:
    [-u --usage]          Display this message
    [-h --help]           Display this message
    Default output directory is the base directory (mostly fmap directory)


  """ % {'EXEC':EXEC_NAME})

def signal_handler(signal,frame):
    print("Your pressed Ctrl+C! Exiting..")
    sys.exit(0)

# Check the number of arguments

if len(sys.argv) < 4:
    help()
    sys.exit(0)

# Default Parameters
echoFile1 = None
echoFile2 = None
phasediff = None
inDir = None

print("\nParsing args   : %s\n" % (sys.argv[ 1: ]) )
sys.stdout.flush()

## Read command line args (getopt or argparser)
try:
    opts, files = getopt.getopt(sys.argv[1:], "i:j:p:k:",
                                ["echoFile1=", "echoFile2=", "phasediff=","inDir="])
except getopt.GetoptError as err:
    help()
    print("ERROR!", err)
    sys.stdout.flush()

for o,a in opts:
    if o in ["-i", "--echoFile1"]:
        echoFile1 = a
    elif o in ["-j", "--echoFile2"]:
        echoFile2 = a
    elif o in ["-p", "--phasediff"]:
        phasediff = a
    elif o in ["-k", "--inDir"]:
        inDir = a
    else:
        help()
        sys.exit(0)

for f in echoFile1, echoFile2,phasediff,inDir:
    if f and not os.path.exists(f):
        print("\nERROR! %s does not exist! Please check" % f)
        sys.exit(0)

def FileExt(FileIP):
        """ To split file base name and extensions """
        dirName, fName = os.path.split(FileIP)
        baseName, fExt = os.path.splitext(fName)
        if FileIP.endswith('nii.gz'):
            baseName, fExt_first = os.path.splitext(baseName)
            fExt = fExt_first + fExt
            return dirName, baseName, fExt
        elif FileIP.endswith('json'):
            return dirName, baseName, fExt


def getjson(FileIP):
    dirName,baseName,fExt = FileExt(FileIP)
    injson = os.path.join(dirName,baseName + '.json')
    return(injson)

"""
Step 1: Rename all files
Siemens should have magnitude1 - shorter echo, magnitude2 - longer echo, phasediff - phase
drift map between echo times. Phase difference json must define two echos
to calculate difference map.

JSON sidecars tells us which is Magnitude and Phasedifference file.
ImageType : 'M' - magnitude
ImageType : 'P' - phasediff
"""
## Getting input json file for echo1 from nifti

injson1 = getjson(echoFile1)

dirN, baseN, fEx = FileExt(injson1)
subS = baseN.rsplit('_',3)[0]

## Getting input json file for echo2 from nifti

injson2 = getjson(echoFile2)

## Read injson1 and injson2

with open(injson1, 'r') as echo1:
    echo1_data = json.load(echo1)
    echoTime1 = echo1_data['EchoTime']

with open(injson2, 'r') as echo2:
    echo2_data = json.load(echo2)
    echoTime2 = echo2_data['EchoTime']

## Read Phase difference file

phasedjson = getjson(phasediff)

os.rename(phasedjson,os.path.join(dirN,subS + '_'+ 'run-01' + '_phasediff' + '.json'))
os.rename(phasediff,os.path.join(dirN,subS + '_'+ 'run-01' + '_phasediff' + '.nii.gz'))
phasedjsonup = os.path.join(dirN,subS + '_'+ 'run-01' + '_phasediff' + '.json')

## Check which echoTime is greater and rename files accordingly
## File which has longer echoTime must be magnitude2 file and is echoTime2
## File which has shorter echoTime must be magnitude1 file and is echoTime1

if echoTime2 > echoTime1:
    print("Echo Time in Echo File 2 is greater than Echo File 1...")

    ## Renaming json and nifti files

    os.rename(injson1,os.path.join(dirN,subS + '_'+ 'run-01' + '_magnitude1' + '.json'))
    os.rename(echoFile1,os.path.join(dirN,subS + '_'+ 'run-01' + '_magnitude1' + '.nii.gz'))

    os.rename(injson2,os.path.join(dirN,subS + '_'+ 'run-01' + '_magnitude2' + '.json'))
    os.rename(echoFile2,os.path.join(dirN,subS + '_'+ 'run-01' + '_magnitude2' + '.nii.gz'))

    ## Add two dict:key values to phasediff json file. EchoTime1 and EchoTime2

    with open(phasedjsonup,'r') as phased:
        phased_data = json.load(phased)
        phased_data['EchoTime1'] = echoTime1
        phased_data['EchoTime2'] = echoTime2
        del phased_data['EchoTime']

    with open(phasedjsonup, 'w') as phased:
        json.dump(phased_data, phased, indent=2)

elif echoTime1 > echoTime2:
    print("Echo Time in Echo File 1 is greater than Echo File 2...")
    print("Echo2 must be longer..Existing file names are incorrect..Renaming..")

    ## Renaming json and nifti files

    os.rename(injson1,os.path.join(dirN,subS + '_'+ 'run-01' + '_magnitude2' + '.json'))
    os.rename(echoFile1,os.path.join(dirN,subS + '_'+ 'run-01' + '_magnitude2' + '.nii.gz'))

    os.rename(injson2,os.path.join(dirN,subS + '_'+ 'run-01' + '_magnitude1' + '.json'))
    os.rename(echoFile2,os.path.join(dirN,subS + '_'+ 'run-01' + '_magnitude1' + '.nii.gz'))

    ## Add two dict:key values to phasediff json file. EchoTime1 and EchoTime2

    with open(phasedjsonup,'r') as phased:
        phased_data = json.load(phased)
        phased_data['EchoTime1'] = echoTime2
        phased_data['EchoTime2'] = echoTime1
        del phased_data['EchoTime']

    with open(phasedjsonup, 'w') as phased:
        json.dump(phased_data, phased, indent=2)

## Read rest state json data in loop and add taskname
## check if slice timing is negative
## check if Multiband acceleration factor exists. If not add for MB scans

rest_dir = os.path.join(inDir,'func')
json_files = [pos_json for pos_json in os.listdir(rest_dir) if pos_json.endswith('.json')]

## getting only multiband files
#mb_files = [mb_json for mb_json in os.listdir(rest_dir) if mb_json.endswith('.json') and 'MB4' in mb_json]

json_rest = [x for x in json_files if "task-rest" in x]
#json_mb_rest = [x for x in mb_files if "task-rest" in x]

# Read single/multi band resting state jsons and check if slice timing has negative values
# if slicetiming is negative, remove those
# if slicetiming is positive, keep it as such
# if multi band, add MultibandAccelerationFactor

for index, js in enumerate(json_rest):
    with open(os.path.join(rest_dir, js),'r') as json_file:
        rest_json = json.load(json_file)
        rest_json['TaskName'] = "rest"

        if "SliceTiming" in rest_json:
            if any(x < 0 for x in rest_json['SliceTiming']):
                print('SliceTiming has negative values..Removing the key from ' + js + '\n')
                del rest_json['SliceTiming']

            else:
                print('SliceTiming has positive values. ' + '\n')
        else:
            print('SliceTiming does not exist for ' + js + '\n')

        if 'MB4' in js:
            print(js + ' is a Multiband sequence. ' + '\n')

            if "MultibandAccelerationFactor" not in rest_json:
                rest_json['MultibandAccelerationFactor'] = 4
        else:
            print(js + ' is a single band sequence.' + '\n')

    with open(os.path.join(rest_dir, js),'w') as json_file:
        json.dump(rest_json, json_file, indent=2)
