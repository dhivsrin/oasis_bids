#!/usr/bin/env python
#-*- coding: utf-8 -*-

###################################################################

__author__ = 'Dhivya Srinivasan'


## Import necessary modules

import numpy as np
import pandas as pd
import os
import sys
import getopt
import json

#####################################################################

def FileExt(FileIP):
        """ To split file base name and extensions """
        dirName, fName = os.path.split(FileIP)
        baseName, fExt = os.path.splitext(fName)
        if FileIP.endswith('nii.gz'):
            baseName, fExt_first = os.path.splitext(baseName)
            fExt = fExt_first + fExt
            return dirName, baseName, fExt
        elif FileIP.endswith('json'):
            return dirName, baseName, fExt

def getjson(FileIP):
    dirName,baseName,fExt = FileExt(FileIP)
    injson = os.path.join(dirName,baseName + '.json')
    return(injson)


#### Err 0 :  Slice Timing Error with Resting State ###
## Remove slicetiming field from json for those who have negative values ##

df = pd.read_csv('/cbica/home/srinivad/Projects/2023_OASIS_BIDS/Err_BIDS/Error0_Slicetiming_json.csv')
df = df.drop_duplicates().reset_index(drop=True)

for js in df['Path']:
	with open(js,'r') as json_file:
		rest_json = json.load(json_file)

		if "SliceTiming" in rest_json:
			if any(x < 0 for x in rest_json['SliceTiming']):
				print('SliceTiming has negative values..Removing key from ' + js + '\n')
				del rest_json['SliceTiming']
			else:
				print('SliceTiming has positive values')
		else:
			print('SliceTiming does not exist for ' + js + '\n')
	with open(js,'w') as json_file:
		json.dump(rest_json,json_file,indent=2)

#### Err 1: Change sess to ses and create bidsignore file for TSE T2w echo names ###

df = pd.read_csv('/cbica/home/srinivad/Projects/2023_OASIS_BIDS/Err_BIDS/Error1_Files_naming_json.csv')
df = df.drop_duplicates().reset_index(drop=True)

## create a new column with changed string and then rename

df_ses = df[df['Path'].str.contains("sess")].reset_index(drop=True)
df_ses['Path_1'] =  df_ses['Path'].str.replace('sess','ses')

names_dict = dict(zip(df_ses['Path'], df_ses['Path_1']))
for old_name,new_name in names_dict.items():
	os.rename(old_name,new_name)

## with TSE in echo in maes - add in bidsignore file
## n=176
df_tse = df[(df['Path'].str.contains('TSE')) & (df['Path'].str.contains('echo'))].reset_index(drop=True)

## added about 74+ DTI files to bidsignore due to volume count mismatch, missing bval and bvecs
## about 17 bvals and bvec files to bidsignore as they have space tab issues 

## Err 7 : TASK_NAME must be defined in resting json file

df = pd.read_csv('/cbica/home/srinivad/Projects/2023_OASIS_BIDS/Err_BIDS/Error7_resting_taskname_define.csv')

df = df.drop_duplicates().reset_index(drop=True)

df['Path_1'] =  df['Path'].str.replace('.nii.gz','.json')

 for js in df['Path_1']:
	with open(js,'r') as json_file:
		rest_json = json.load(json_file)
		rest_json['TaskName'] = "rest"
	with open(js,'w') as json_file:
		json.dump(rest_json,json_file,indent=2)



